/**
 * Created with Goland.
 * Description:
 * <p> 给定一个整数数组 nums 和一个目标值 target，请你在该数组中找出和为目标值的那 两个 整数，并返回他们的数组下标。
 * <p> 你可以假设每种输入只会对应一个答案。但是，数组中同一个元素不能使用两遍。
 * <p> 给定 nums = [2, 7, 11, 15], target = 9
 * <p> 因为 nums[0] + nums[1] = 2 + 7 = 9
 * <p> 所以返回 [0, 1]
 * @author nonser
 * @date 2020/7/15 10:57
 * @version 1.0
 */
package __两数之和

// 暴力法，双重for循环
func twoSum(nums []int, target int) []int {
	if nums != nil {
		for index := 0; index < len(nums); index ++ {
			for n := index + 1; n < len(nums); n ++ {
				if (nums[index] + nums[n]) == target {
					return []int{index, n}
				}
			}
		}
	}
	return nil
}

// 暴力法，双重for循环 range
func twoSum2(nums []int, target int) []int {
	if nums != nil {
		for index, num :=  range nums{
			for n := index + 1; n < len(nums); n ++ {
				if (num + nums[n]) == target {
					return []int{index, n}
				}
			}
		}
	}
	return nil
}

// 两遍哈希表
func twoSum3(nums []int, target int) []int {
	if nums != nil {
		var mmap = make(map[int]int)
		for index, num :=  range nums{
			mmap[num] = index
		}
		for index, num :=  range nums{
			value := target - num
			if v, ok := mmap[value]; ok && v != index{
				return []int{index, v}
			}
		}
	}
	return nil
}

// 一遍哈希表
func twoSum4(nums []int, target int) []int {
	if nums != nil {
		var mmap = make(map[int]int)
		for index, num :=  range nums{
			value := target - num
			if v, ok := mmap[value]; ok && v != index{
				return []int{v, index}
			}
			mmap[num] = index
		}
	}
	return nil
}