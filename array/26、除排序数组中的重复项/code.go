/**
 * Created with Goland.
 * Description:
 * <p> 给定一个排序数组，你需要在 原地 删除重复出现的元素，使得每个元素只出现一次，返回移除后数组的新长度。
 * <p> 不要使用额外的数组空间，你必须在 原地 修改输入数组 并在使用 O(1) 额外空间的条件下完成。
 * @author nonser
 * @date 2020/7/15 10:57
 * @version 1.0
 */
package __除排序数组中的重复项


func removeDuplicates(nums []int) int {
	if len(nums) < 2 {
		return len(nums)
	}
	tmp := nums[0]
	length := 1
	for i := 1; i < len(nums); i++ {
		if tmp != nums[i] {
			tmp = nums[i]
			nums[length] = nums[i]
			length++
		}
	}
	return length
}

// 双指针法
// 数组完成排序后，我们可以放置两个指针 ii 和 jj，其中 ii 是慢指针，而 jj 是快指针。只要 nums[i] == nums[j]，我们就增加 jj 以跳过重复项。
// 当我们遇到 nums[j] != nums[i] 时，跳过重复项的运行已经结束，因此我们必须把它（nums[j]nums[j]）的值复制到 nums[i + 1]。然后递增 ii，接着我们将再次重复相同的过程，直到 jj 到达数组的末尾为止。
func removeDuplicates2(nums []int) int {
	if len(nums) < 2 {
		return len(nums)
	}

	i := 0
	for j := 1; j < len(nums); j++ {
		if nums[i] != nums[j] {
			i++
			nums[i] = nums[j]
		}
	}
	return i+1
}